
digraph {{ .Top.Name }} {
	{{ range .Rules }}	
		{{ .Name }} -> 
		{{- range .Definition.Sequences }}
			{{ . }}
		{{ end -}}
		;
	{{ end }}
}


